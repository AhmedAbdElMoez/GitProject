#include <iostream>
#include <vector>
using namespace std;

template <typename ValueType>
class Set
{
private:
	vector<ValueType> vec;
public:
	Set();
	~Set();
	int size() const
	{
		return vec.size();
	}
	bool isEmpty() const
	{
		return vec.empty();
	}
	void insert(const ValueType& value)
	{
		bool exit = false;
		for(int x=0 ; x<vec.size() ; x++)
		{
			ValueType V = vec[x];
			if(V == value)
			{
				cout << "Cannot add duplicate numbers";
				exit = true;
			}
		}
		if(exit == true)
		{
			return;
		}
		else
		{
			vec.push_back(value);
			sort(vec.begin() , vec.end());
		}
	}
	void remove(const ValueType& value)
	{
		for(int x=0 ; x<vec.size() ; x++)
		{
			ValueType V = vec[x];
			if(V == value)
			{
				vec.erase(vec.begin() + x);
				//vec.erase()
			}
		}
	}
	bool contains(const ValueType& value) const
	{
		for(int x=0 ; x<vec.size() ; x++)
		{
			ValueType V = vec[x];
			if(V == value)
			{
				return true;
			}
		}
		return false;
	}
	bool isSubsetOf(const Set& set2) const
	{
		int counter = 0;
		for(int x=0 ; x<vec.size() ; x++)
		{
			ValueType V1 = vec[x];
			for(int y=0 ; y<set2.vec.size() ; y++)
			{
				ValueType V2 = set2.vec[y];
				if(V1 == V2)
				{
					counter++;
				}
			}
		}
		if(counter == vec.size())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	void clear()
	{
		vec.clear();
	}
};

int main()
{
	Set<int> test;
	test.insert(78);
	test.insert(3);
	test.insert(5);
	cout << "Set size: " << test.size();
	bool isEmpty = test.isEmpty();
	if(isEmpty == true)
	{
		cout << "Empty set\n";
	}
	else
	{
		cout << "Occupied set\n";
	}
	test.remove(5);
	cout << "Set size: " << test.size();
	bool exist = test.contains(3);
	if(exist == true)
	{
		cout << "The set contains this number\n";
	}
	else
	{
		cout << "The set doesn't contain this number\n";
	}
	test.clear();
	cout << "Set size: " << test.size();

	Set<int> test2;
	test2.insert(3);
	test2.insert(5);
	bool subset = test2.isSubsetOf(test);
	if(subset == true)
	{
		cout << "The second set is subset of the first set\n";
	}
	else
	{
		cout << "The second set is not subset of the first set\n";
	}

	return 0;
}

#include <iostream>
#include <vector>
using namespace std;

template <typename ValueType>
class Set
{
private:
	vector<ValueType> vec;
public:
	Set();
	~Set();
	int size() const
	{
		return vec.size();
	}
	bool isEmpty() const
	{
		return vec.empty();
	}
	void insert(const ValueType& value)
	{
		bool exit = false;
		for(int x=0 ; x<vec.size() ; x++)
		{
			ValueType V = vec[x];
			if(V == value)
			{
				cout << "Cannot add duplicate numbers";
				exit = true;
			}
		}
		if(exit == true)
		{
			return;
		}
		else
		{
			vec.push_back(value);
			sort(vec.begin() , vec.end());
		}
	}
	void remove(const ValueType& value)
	{
		for(int x=0 ; x<vec.size() ; x++)
		{
			ValueType V = vec[x];
			if(V == value)
			{
				vec.erase(vec.begin() + x);
				//vec.erase()
			}
		}
	}
	bool contains(const ValueType& value) const
	{
		for(int x=0 ; x<vec.size() ; x++)
		{
			ValueType V = vec[x];
			if(V == value)
			{
				return true;
			}
		}
		return false;
	}
	bool isSubsetOf(const Set& set2) const
	{
		int counter = 0;
		for(int x=0 ; x<vec.size() ; x++)
		{
			ValueType V1 = vec[x];
			for(int y=0 ; y<set2.vec.size() ; y++)
			{
				ValueType V2 = set2.vec[y];
				if(V1 == V2)
				{
					counter++;
				}
			}
		}
		if(counter == vec.size())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	void clear()
	{
		vec.clear();
	}
	bool operator==(const Set& set2) const
	{
		bool sameSet = false;
		int counter = 0;

		if(vec.size() == set2.vec.size())
		{
			for(int x=0 ; x<vec.size() ; x++)
			{
				if(vec[x] == set2.vec[x])
				{
					counter++;
				}
			}
			if(counter == vec.size())
			{
				sameSet = true;
			}
		}

		if(sameSet)
		  return true;
		else
		  return false;
	}
	bool operator!=(const Set& set2) const
	{
		bool sameSet = false;
		int counter = 0;

		if(vec.size() == set2.vec.size())
		{
			for(int x=0 ; x<vec.size() ; x++)
			{
				if(vec[x] == set2.vec[x])
				{
					counter++;
				}
			}
			if(counter == vec.size())
			{
				sameSet = true;
			}
		}

		if(sameSet)
		  return false;
		else
		  return true;
		}
	Set operator+ (const Set& set2) const
	{
		Set newSet;
		for(int x=0 ; x<vec.size() ; x++)
		{
			ValueType V = vec[x];
			newSet.insert(V);
		}
		for(int x=0 ; x<set2.vec.size() ; x++)
		{
			ValueType V = set2.vec[x];
			newSet.insert(V);
		}
		return newSet;
	}
	Set operator* (const Set& set2) const
	{
		Set newSet;
		for(int x=0 ; x<vec.size() ; x++)
		{
			ValueType V1 = vec[x];
			for(int x=0 ; x<set2.vec.size() ; x++)
			{
				ValueType V2 = set2.vec[x];
				if(V1 == V2)
				{
					newSet.insert(V1);
				}
			}
		}
		return newSet;
	}
	Set operator- (const Set& set2) const
	{
		Set newSet;
		int min = vec.size();
		if(vec.size() > set2.vec.size())
		{
			min = set2.vec.size();
		}
		else if(vec.size() < set2.vec.size())
		{
			min = vec.size();
		}
		for(int x=0 ; x<min ; x++)
		{
			ValueType V1 = vec[x];
			ValueType V2 = set2.vec[x];
			ValueType V3 = V1-V2;
			newSet.insert(V3);
		}
		return newSet;
	}
	Set& operator=(const Set& other)
	{
		vec = other.vec;
		return *this;
	}
};

int main()
{
	Set<int> test;
	test.insert(1);
	test.insert(3);
	test.insert(5);
	Set<int> test2;
	test2.insert(2);
	test2.insert(4);
	test2.insert(5);

	if(test == test2)
	{
		cout << "They are equal\n";
	}
	else
	{
		cout << "They are not equal\n";
	}

	if(test != test2)
	{
		cout << "They are not equal\n";
	}
	else
	{
		cout << "They are equal\n";
	}

	Set<int> unionSet = test + test2;
	Set<int> interSet = test * test2;
	Set<int> diffSet = test - test2;

	return 0;
}
